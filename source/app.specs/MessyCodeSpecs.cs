﻿using System;
using Machine.Specifications;
using app.refactor_this;
using developwithpassion.specifications.rhinomocks;

namespace app.specs
{
	[Subject(typeof (MessyCode))]
	public class MessyCodeSpecs
	{
		public abstract class concern : Observes<MessyCode>
		{
		}

		public class when_determining_if_the_date_is_valid : concern
		{
			private Establish c = () =>
				{
					current_requisition = new Requisition();
					GetCurrentRequisition_Behaviour item = () => current_requisition;
					spec.change(() => MessyCode.current_requisition).to(item);
				};

			private Because b = () =>
			                    result = sut.IsDateTimeValid();

			public class and_there_is_no_from_date
			{
				private Establish c = () =>
					{
						current_requisition.DateFrom = null;
					};

				public class and_no_other_date_information_is_present
				{
					private Establish c = () =>
					{
						GetLocalizedValueByKey_Behaviour key_fetch = x =>
						{
							x.ShouldEqual(LocalizationKeys.bookperson_entertodate);
							return "blah";
						};
						AddApplicationMessage message_add = (message, type, error_type) =>
						{
							message.ShouldEqual("blah");
							type.ShouldEqual(MessageType.popup);
							error_type.ShouldEqual(MessageType.error);
							return 0;
						};
						spec.change(() => MessyCode.localization_resolution).to(key_fetch);
						spec.change(() => MessyCode.message_addition).to(message_add);
					};

					private It should_be_valid = () =>
					                             result.ShouldBeTrue();
				}
			}

			public class and_there_is_a_from_date
			{
				private Establish c = () =>
					{
						current_requisition.DateFrom = new DateTime(2011, 10, 10);
					};

				public class and_no_to_date_has_been_provided
				{
					private Establish c = () =>
						{
							GetLocalizedValueByKey_Behaviour key_fetch = x =>
								{
									x.ShouldEqual(LocalizationKeys.bookperson_entertodate);
									return "blah";
								};
							AddApplicationMessage message_add = (message, type, error_type) =>
								{
									message.ShouldEqual("blah");
									type.ShouldEqual(MessageType.popup);
									error_type.ShouldEqual(MessageType.error);
									return 0;
								};
							spec.change(() => MessyCode.localization_resolution).to(key_fetch);
							spec.change(() => MessyCode.message_addition).to(message_add);
						};

					private Because b = () =>
												 result = sut.IsDateTimeValid();


					private It should_be_invalid = () =>
					                               result.ShouldBeFalse();
				}

				public class and_a_to_date_has_been_provided
				{
					public class and_the_from_date_is_less_than_the_to_date
					{
						private Establish c = () =>
							{
								GetLocalizedValueByKey_Behaviour key_fetch = x =>
									{
										x.ShouldEqual(LocalizationKeys.bookperson_fromdateisgreater);
										return "blah";
									};
								AddApplicationMessage message_add = (message, type, error_type) =>
									{
										message.ShouldEqual("blah");
										type.ShouldEqual(MessageType.popup);
										error_type.ShouldEqual(MessageType.error);
										return 0;
									};
								spec.change(() => MessyCode.localization_resolution).to(key_fetch);
								spec.change(() => MessyCode.message_addition).to(message_add);

								var dtnow = System.DateTime.Now;
								current_requisition.DateTo = dtnow.AddDays(-1);
								current_requisition.DateFrom = dtnow;
							};

						private Because b = () =>
						                    result = sut.IsDateTimeValid();

						private It should_be_invalid = () => result.ShouldBeFalse();
					}

					public class and_the_from_date_is_greater_than_the_to_date
					{
						public class and_the_to_date_is_greater_than_or_equal_to_ten_years
						{
							private Establish c = () =>
								{
									GetLocalizedValueByKey_Behaviour key_fetch = x =>
										{
											x.ShouldEqual(LocalizationKeys.bookperson_distantfuture);
											return "blah";
										};

									AddApplicationMessage message_add = (message, type, error_type) =>
										{
											message.ShouldEqual("blah");
											type.ShouldEqual(MessageType.popup);
											error_type.ShouldEqual(MessageType.error);
											return 0;
										};
									spec.change(() => MessyCode.localization_resolution).to(key_fetch);
									spec.change(() => MessyCode.message_addition).to(message_add);

									var dtnow = System.DateTime.Now;
									current_requisition.DateFrom = dtnow.AddYears(11);
									current_requisition.DateTo = dtnow.AddYears(12);
								};

							private Because b = () =>
							                    result = sut.IsDateTimeValid();

							private It should_be_invalid = () => result.ShouldBeFalse();
						}

						public class and_the_to_date_is_greater_than_or_eqaul_to_one_hundred_days
						{
							private Establish c = () =>
								{
									var dtnow = System.DateTime.Now;
									current_requisition.DateTo = dtnow.AddDays(100);
									current_requisition.DateFrom = dtnow;

									GetLocalizedValueByKey_Behaviour key_fetch = x =>
										{
											x.ShouldEqual(LocalizationKeys.bookperson_datedifference);
											return "blah 100";
										};
									AddApplicationMessage message_add = (message, type, error_type) =>
										{
											message.ShouldEqual(string.Format("blah {0}",
											                                  current_requisition.DateTo.Value.Subtract(
												                                  current_requisition.DateFrom.Value).TotalDays.ToString()));
											type.ShouldEqual(MessageType.popup);
											error_type.ShouldEqual(MessageType.error);
											return 0;
										};
									spec.change(() => MessyCode.localization_resolution).to(key_fetch);
									spec.change(() => MessyCode.message_addition).to(message_add);
								};

							private Because b = () =>
							                    result = sut.IsDateTimeValid();

							private It should_be_invalid = () => result.ShouldBeFalse();
						}

						public class and_the_to_date_is_less_than_one_hundred_days
						{
							private Establish c = () =>
								{
									var dtnow = System.DateTime.Now;
									current_requisition.DateTo = dtnow.AddDays(99);
									current_requisition.DateFrom = dtnow;
								};

							private Because b = () =>
							                    result = sut.IsDateTimeValid();

							private It should_be_valid = () => result.ShouldBeTrue();
						}

					}

					public class and_the_from_date_equals_the_to_date
					{
						private Establish c = () =>
							{
								var dtnow = System.DateTime.Now;
								current_requisition.DateTo = dtnow;
								current_requisition.DateFrom = dtnow;

							};

						public class and_the_time_from_is_not_provided
						{
							private Because b = () =>
								{
									sut.TimeFrom = string.Empty;
									sut.TimeTo = "12:00";
									result = sut.IsDateTimeValid();
								};

							private It should_be_valid = () =>
							                             result.ShouldBeTrue();

						}

						public class and_the_time_from_is_provided_and_the_time_to_is_not_provided
						{

							private Establish c = () =>
								{
									GetLocalizedValueByKey_Behaviour key_fetch = x =>
										{
											x.ShouldEqual(LocalizationKeys.bookperson_entertotime);
											return "blah";
										};
									AddApplicationMessage message_add = (message, type, error_type) =>
										{
											message.ShouldEqual("blah");
											type.ShouldEqual(MessageType.popup);
											error_type.ShouldEqual(MessageType.error);
											return 0;
										};
									spec.change(() => MessyCode.localization_resolution).to(key_fetch);
									spec.change(() => MessyCode.message_addition).to(message_add);

									var dtnow = System.DateTime.Now;
									current_requisition.DateTo = dtnow;
									current_requisition.DateFrom = dtnow;
								};


							private Because b = () =>
								{
									sut.TimeFrom = "12:00";
									sut.TimeTo = string.Empty;
									result = sut.IsDateTimeValid();
								};


							private It should_be_invalid = () =>
							                               result.ShouldBeFalse();
						}

						public class and_the_time_from_is_provided_and_the_time_to_is_provided
						{
							public class and_both_time_from_and_time_to_hours_and_minutes_are_zero
							{
								private Because b = () =>
									{
										sut.TimeFrom = "00:00";
										sut.TimeTo = "00:00";
										result = sut.IsDateTimeValid();
									};


								private It should_be_valid = () =>
								                             result.ShouldBeTrue();

							}

							public class and_both_time_from_and_time_to_hours_and_minutes_are_equal
							{
								private Establish c = () =>
									{
										GetLocalizedValueByKey_Behaviour key_fetch = x =>
											{
												x.ShouldEqual(LocalizationKeys.bookperson_timeissame);
												return "blah";
											};
										AddApplicationMessage message_add = (message, type, error_type) =>
											{
												message.ShouldEqual("blah");
												type.ShouldEqual(MessageType.popup);
												error_type.ShouldEqual(MessageType.error);
												return 0;
											};
										spec.change(() => MessyCode.localization_resolution).to(key_fetch);
										spec.change(() => MessyCode.message_addition).to(message_add);

										var dtnow = System.DateTime.Now;
										current_requisition.DateTo = dtnow;
										current_requisition.DateFrom = dtnow;
									};


								private Because b = () =>
									{
										sut.TimeFrom = "12:34";
										sut.TimeTo = "12:34";
										result = sut.IsDateTimeValid();
									};


								private It should_be_invalid = () =>
								                               result.ShouldBeFalse();

							}

							public class and_the_time_to_minutes_are_before_the_time_from_minutes
							{
								public class and_the_time_to_hours_and_the_time_from_hours_are_equal
								{
									private Establish c = () =>
										{
											GetLocalizedValueByKey_Behaviour key_fetch = x =>
												{
													x.ShouldEqual(LocalizationKeys.bookperson_totimeislesser);
													return "blah";
												};
											AddApplicationMessage message_add = (message, type, error_type) =>
												{
													message.ShouldEqual("blah");
													type.ShouldEqual(MessageType.popup);
													error_type.ShouldEqual(MessageType.error);
													return 0;
												};
											spec.change(() => MessyCode.localization_resolution).to(key_fetch);
											spec.change(() => MessyCode.message_addition).to(message_add);

											var dtnow = System.DateTime.Now;
											current_requisition.DateTo = dtnow;
											current_requisition.DateFrom = dtnow;
										};


									private Because b = () =>
										{
											sut.TimeFrom = "11:12";
											sut.TimeTo = "11:11";
											result = sut.IsDateTimeValid();
										};

									private It should_be_invalid = () =>
									                               result.ShouldBeFalse();
								}

								public class and_the_time_to_hours_are_before_time_from_hours
								{
									private Establish c = () =>
										{
											GetLocalizedValueByKey_Behaviour key_fetch = x =>
												{
													x.ShouldEqual(LocalizationKeys.bookperson_totimeislesser);
													return "blah";
												};
											AddApplicationMessage message_add = (message, type, error_type) =>
												{
													message.ShouldEqual("blah");
													type.ShouldEqual(MessageType.popup);
													error_type.ShouldEqual(MessageType.error);
													return 0;
												};
											spec.change(() => MessyCode.localization_resolution).to(key_fetch);
											spec.change(() => MessyCode.message_addition).to(message_add);

											var dtnow = System.DateTime.Now;
											current_requisition.DateTo = dtnow;
											current_requisition.DateFrom = dtnow;
										};

									private Because b = () =>
										{
											sut.TimeFrom = "12:12";
											sut.TimeTo = "11:11";
											result = sut.IsDateTimeValid();
										};

									private It should_be_invalid = () =>
									                               result.ShouldBeFalse();
								}

								public class and_the_time_to_hours_are_after_time_from_hours
								{

									private Because b = () =>
										{
											sut.TimeFrom = "11:12";
											sut.TimeTo = "12:11";
											result = sut.IsDateTimeValid();

										};

									private It should_be_valid = () =>
									                             result.ShouldBeTrue();
								}
							}

							public class and_the_time_to_minutes_are_after_the_time_from_minutes
							{
								public class and_the_time_to_hours_and_the_time_from_hours_are_equal
								{
									private Because b = () =>
										{
											sut.TimeFrom = "11:11";
											sut.TimeTo = "11:12";
											result = sut.IsDateTimeValid();
										};

									private It should_be_valid_hours_equal_when_mins_after = () =>
									                                                         result.ShouldBeTrue();
								}

								public class and_the_time_to_hours_are_before_time_from_hours
								{
									private Establish c = () =>
										{
											GetLocalizedValueByKey_Behaviour key_fetch = x =>
												{
													x.ShouldEqual(LocalizationKeys.bookperson_totimeislesser);
													return "blah";
												};
											AddApplicationMessage message_add = (message, type, error_type) =>
												{
													message.ShouldEqual("blah");
													type.ShouldEqual(MessageType.popup);
													error_type.ShouldEqual(MessageType.error);
													return 0;
												};
											spec.change(() => MessyCode.localization_resolution).to(key_fetch);
											spec.change(() => MessyCode.message_addition).to(message_add);

											var dtnow = System.DateTime.Now;
											current_requisition.DateTo = dtnow;
											current_requisition.DateFrom = dtnow;
										};

									private Because b = () =>
										{
											sut.TimeFrom = "12:11";
											sut.TimeTo = "11:12";
											result = sut.IsDateTimeValid();
										};

									private It should_be_invalid_hours_before_when_mins_after = () =>
									                                                            result.ShouldBeFalse();
								}

								public class and_the_time_to_hours_are_after_time_from_hours
								{

									private Because b = () =>
										{
											sut.TimeFrom = "11:11";
											sut.TimeTo = "12:12";
											result = sut.IsDateTimeValid();

										};

									private It should_be_valid_hours_after_when_mins_after = () =>
									                                                         result.ShouldBeTrue();
								}
							}
						}

						private Because b = () =>
													 result = sut.IsDateTimeValid();

						private It should_be_valid = () => result.ShouldBeTrue();

					}
				}
			}

			private static bool result;
			private static Requisition current_requisition;
		}
	}
}