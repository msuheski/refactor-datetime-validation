using System;

namespace app.refactor_this
{
	public class Requisition
	{
		public DateTime? DateTo { get; set; }
		public DateTime? DateFrom { get; set; }
	}
}