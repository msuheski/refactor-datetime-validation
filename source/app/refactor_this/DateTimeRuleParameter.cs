using System;

namespace app.refactor_this
{
	public class DateTimeRuleParameter
	{
		public bool Result { get; set; }
		public bool StopValidating { get; set; }
		public DateTime? CDateFrom { get; set; }
		public DateTime? CDateTo { get; set; }
		public string TimeFrom { get; set; }
		public string TimeTo { get; set; }

		public DateTimeRuleParameter()
		{
		}
	}
}