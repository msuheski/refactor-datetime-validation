﻿using System;
using System.Collections.Generic;
using app.refactor_this.rules;

namespace app.refactor_this
{
	public delegate Requisition GetCurrentRequisition_Behaviour();

	public delegate object GetLocalizedValueByKey_Behaviour(string key);

	public delegate int AddApplicationMessage(string message, string dialog_type, string error_type);

	public class LocalizationKeys
	{
		public const string bookperson_entertodate = "BookPerson-EnterToDate";
		public const string bookperson_fromdateisgreater = "BookPerson-FromDateIsGreater";
		public const string bookperson_distantfuture = "BookPerson-DistantFuture";
		public const string bookperson_datedifference = "BookPerson-DateDifference";
		public const string bookperson_entertotime = "BookPerson-EnterToTime";
		public const string bookperson_timeissame = "BookPerson-TimeIsSame";
		public const string bookperson_totimeislesser = "BookPerson-ToTimeIsLesser";
	}

	public class MessyCode
	{
		public string TimeFrom = null;
		public string TimeTo = null;
		public static GetCurrentRequisition_Behaviour current_requisition = () => BookPerson.requisition;
		public static GetLocalizedValueByKey_Behaviour localization_resolution = LocalizationManager.GetValue;
		public static AddApplicationMessage message_addition = ApplicationMessages.AddMessage;

		public bool IsDateTimeValid()
		{
			var current = current_requisition();
			var dateTimeRuleParameter = new DateTimeRuleParameter()
			{
				CDateFrom = current.DateFrom,
				CDateTo = current.DateTo,
				TimeFrom = TimeFrom,
				TimeTo = TimeTo,
				StopValidating = false
			};

			if (current.DateFrom == null) return no_extra_date_information_has_been_provided(current);

			List<IDateTimeRule> rulelist = new List<IDateTimeRule>
			{
				new ToDateIsNullRule(),
				new FromDateLessThanToDateRule(),
				new ToDateIsGreaterThan10YearsRule(),
				new ToDateIsGreaterThan100DaysRule(),
				new TimeFromIsEmptyStringRule(),
				new TimeToIsEmptyStringRule(),
				new FromDateDoesNotEqualToDateRule(),
				new ToTimeFromTimeZeroRule(),
				new TimeToEqualsTimeFromRule(),
				new TimeToLessThanFromTimeRule()
			};

			bool result = true;

			rulelist.ForEach(rule =>
				{
					if (!dateTimeRuleParameter.StopValidating)
						result = rule.Validate(dateTimeRuleParameter, localization_resolution, message_addition);
				});

			return result;
		}

		public bool IsDateTimeValidOLD()
		{
			var current = current_requisition();

			DateTimeRuleParameter dateTimeRuleParameter = new DateTimeRuleParameter()
				{
					CDateFrom = current.DateFrom,
					CDateTo = current.DateTo,
					TimeFrom = TimeFrom,
					TimeTo = TimeTo,
					Result = false,
					StopValidating = true
				};

			if (current.DateFrom == null) return no_extra_date_information_has_been_provided(current);

			bool result = true;
			IDateTimeRule rule;

			rule = new ToDateIsNullRule();
			result = rule.Validate(dateTimeRuleParameter, localization_resolution, message_addition);
			if (dateTimeRuleParameter.StopValidating)
				return result;

			rule = new FromDateLessThanToDateRule();
			result = rule.Validate(dateTimeRuleParameter, localization_resolution, message_addition);
			if (dateTimeRuleParameter.StopValidating)
				return result;

			rule = new ToDateIsGreaterThan10YearsRule();
			result = rule.Validate(dateTimeRuleParameter, localization_resolution, message_addition);
			if (dateTimeRuleParameter.StopValidating)
				return result;

			rule = new ToDateIsGreaterThan100DaysRule();
			result = rule.Validate(dateTimeRuleParameter, localization_resolution, message_addition);
			if (dateTimeRuleParameter.StopValidating)
				return result;

			rule = new TimeFromIsEmptyStringRule();
			result = rule.Validate(dateTimeRuleParameter, localization_resolution, message_addition);
			if (dateTimeRuleParameter.StopValidating)
				return result;

			rule = new TimeToIsEmptyStringRule();
			result = rule.Validate(dateTimeRuleParameter, localization_resolution, message_addition);
			if (dateTimeRuleParameter.StopValidating)
				return result;

			rule = new FromDateDoesNotEqualToDateRule();
			result = rule.Validate(dateTimeRuleParameter, localization_resolution, message_addition);
			if (dateTimeRuleParameter.StopValidating)
				return result;

			rule = new ToTimeFromTimeZeroRule();
			result = rule.Validate(dateTimeRuleParameter, localization_resolution, message_addition);
			if (dateTimeRuleParameter.StopValidating)
				return result;

			rule = new TimeToEqualsTimeFromRule();
			result = rule.Validate(dateTimeRuleParameter, localization_resolution, message_addition);
			if (dateTimeRuleParameter.StopValidating)
				return result;

			rule = new TimeToLessThanFromTimeRule();
			result = rule.Validate(dateTimeRuleParameter, localization_resolution, message_addition);
			if (dateTimeRuleParameter.StopValidating)
				return result;

			//ToTime greater than FromTime
			return true;
		}

		bool check_condition_and_add_message<T>(Predicate<T> predicate, T actual, string localization_name,
                                            string dialogType, string errorType)
    {
      if (predicate(actual))
      {
        message_addition(localization_resolution(localization_name).ToString(),
                         dialogType,
                         errorType);
        return true;
      }

      return false;
    }

    bool no_extra_date_information_has_been_provided(Requisition current)
    {
      return current.DateFrom == null && current.DateTo == null && TimeFrom == string.Empty &&
        TimeTo == string.Empty;
    }
  }
}