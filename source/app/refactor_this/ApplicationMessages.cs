namespace app.refactor_this
{
	public class ApplicationMessages
	{
		public static int AddMessage(string message, string message_type, string error)
		{
			if (message_type == "popup")
			{
				MessageDialog.show_error_popup(message, error);
				return 1;
			}
			if (message_type == "log_message")
			{
				LogManager.log_error(message, error);
				return 2;
			}
			if (message_type == "db_message")
			{
				DBLogger.log_error(message, error);
				return 3;
			}
			return 0;
			;
		}
	}
}