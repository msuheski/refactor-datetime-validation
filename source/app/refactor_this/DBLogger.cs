using System;
using System.Data;
using System.Data.SqlClient;

namespace app.refactor_this
{
	public class DBLogger
	{
		public static void log_error(string message, string error)
		{
			var connection = new SqlConnection("data source=(local);Integrated Security=SSPI;Initial Catalog=errors");
			connection.Open();
			var command = connection.CreateCommand();
			command.CommandText = "InsertErrorMessage";
			command.CommandType = CommandType.StoredProcedure;

			var message_parameter = command.CreateParameter();
			message_parameter.ParameterName = "@p_message";
			message_parameter.Value = message;

			var error_parameter = command.CreateParameter();
			error_parameter.ParameterName = "@p_error";
			error_parameter.Value = error;

			command.Parameters.Add(message_parameter);
			command.Parameters.Add(error_parameter);

			try
			{
				command.ExecuteNonQuery();
			}
			catch (Exception ex)
			{
				LogManager.log_error("Logging failure", "Failure to log error message");
			}
			finally
			{
				connection.Close();
			}
		}
	}
}