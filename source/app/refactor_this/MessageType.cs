namespace app.refactor_this
{
	public class MessageType
	{
		public const string log_message = "log_message";
		public const string db_message = "db_message";
		public const string error = "error";
		public const string popup = "PopUp";
	}
}