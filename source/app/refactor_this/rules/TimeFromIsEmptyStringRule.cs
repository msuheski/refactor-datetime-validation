﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace app.refactor_this.rules
{
	class TimeFromIsEmptyStringRule :IDateTimeRule
	{
		public bool Validate(DateTimeRuleParameter dateTimeRuleParameter, GetLocalizedValueByKey_Behaviour localizationManager, AddApplicationMessage messageAdd)
		{
			
			if(dateTimeRuleParameter.TimeFrom == string.Empty)
			{
				dateTimeRuleParameter.StopValidating = true;
				return true;
			}

				dateTimeRuleParameter.StopValidating = false;
			return false;

		}

		public string GetErrorMessage()
		{
			return string.Empty;
		}
	}
}
