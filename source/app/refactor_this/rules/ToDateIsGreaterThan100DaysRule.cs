using System;

namespace app.refactor_this.rules
{
	public class ToDateIsGreaterThan100DaysRule : IDateTimeRule
	{
		public bool Validate(DateTimeRuleParameter dateTimeRuleParameter, GetLocalizedValueByKey_Behaviour localizationManager, AddApplicationMessage messageAdd)
		{
			if(!(dateTimeRuleParameter.CDateTo.Value.Subtract(dateTimeRuleParameter.CDateFrom.Value).TotalDays < 100))
			{
				var message = localizationManager(GetErrorMessage()).ToString();
				message = string.Format(message, dateTimeRuleParameter.CDateTo.Value.Subtract(dateTimeRuleParameter.CDateFrom.Value).TotalDays.ToString());
				messageAdd(message, MessageType.popup, MessageType.error);
				dateTimeRuleParameter.StopValidating = true;
				return false;
			}
			dateTimeRuleParameter.StopValidating = false;
			return true;
		}

		public string GetErrorMessage()
		{
			return LocalizationKeys.bookperson_datedifference;
		}
	}
}