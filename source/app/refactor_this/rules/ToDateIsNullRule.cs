﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace app.refactor_this.rules
{
	public class ToDateIsNullRule :IDateTimeRule
	{
		public bool Validate(DateTimeRuleParameter dateTimeRuleParameter, GetLocalizedValueByKey_Behaviour localizationManager, AddApplicationMessage messageAdd)
		{
			if (dateTimeRuleParameter.CDateTo == null)
			{
				messageAdd(localizationManager(GetErrorMessage()).ToString(), MessageType.popup, MessageType.error);
				dateTimeRuleParameter.StopValidating = true;
				return false;
			}

			dateTimeRuleParameter.StopValidating = false;
			return true;
		}

		public string GetErrorMessage()
		{
			return LocalizationKeys.bookperson_entertodate;
		}
	}
}
