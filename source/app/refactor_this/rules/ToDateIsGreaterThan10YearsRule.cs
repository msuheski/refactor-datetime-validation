using System;

namespace app.refactor_this.rules
{
	public class ToDateIsGreaterThan10YearsRule : IDateTimeRule
	{
		public DateTimeRuleParameter Parameters { get; set; }

		public bool Validate(DateTimeRuleParameter dateTimeRuleParameter, GetLocalizedValueByKey_Behaviour localizationManager, AddApplicationMessage messageAdd)
		{
			if ((dateTimeRuleParameter.CDateTo.Value.Year - Today.Year) >= 10)
			{
				messageAdd(localizationManager(GetErrorMessage()).ToString(), MessageType.popup, MessageType.error);
				dateTimeRuleParameter.StopValidating = true;
				return false;
			}
			dateTimeRuleParameter.StopValidating = false;
			return true;
		}

		public string GetErrorMessage()
		{
			return LocalizationKeys.bookperson_distantfuture;
		}
	}
}