using System;

namespace app.refactor_this.rules
{
	public class FromDateLessThanToDateRule : IDateTimeRule
	{
		public bool Validate(DateTimeRuleParameter dateTimeRuleParameter, GetLocalizedValueByKey_Behaviour localizationManager, AddApplicationMessage messageAdd)
		{
			if (DateTime.Compare(dateTimeRuleParameter.CDateFrom.Value, dateTimeRuleParameter.CDateTo.Value) > 0)
			{
				messageAdd(localizationManager(GetErrorMessage()).ToString(), MessageType.popup, MessageType.error);
				dateTimeRuleParameter.StopValidating = true;
				return false;
			}

			dateTimeRuleParameter.StopValidating = false;
			return true;
		}

		public string GetErrorMessage()
		{
			return LocalizationKeys.bookperson_fromdateisgreater;
		}
	}
}
