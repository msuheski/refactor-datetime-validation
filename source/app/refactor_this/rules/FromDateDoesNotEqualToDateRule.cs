using System;

namespace app.refactor_this.rules
{
	public class FromDateDoesNotEqualToDateRule : IDateTimeRule
	{
		public bool Validate(DateTimeRuleParameter dateTimeRuleParameter, GetLocalizedValueByKey_Behaviour localizationManager, AddApplicationMessage messageAdd)
		{
			if (DateTime.Compare(dateTimeRuleParameter.CDateFrom.Value, dateTimeRuleParameter.CDateTo.Value) != 0)
			{
				dateTimeRuleParameter.StopValidating = true;
				return true;
			}

			dateTimeRuleParameter.StopValidating = false;
			return false;
		}

		public string GetErrorMessage()
		{
			return string.Empty;
		}
	} 
}