namespace app.refactor_this.rules
{
	public interface IDateTimeRule
	{
		bool Validate(DateTimeRuleParameter dateTimeRuleParameter, GetLocalizedValueByKey_Behaviour localizationManager, AddApplicationMessage messageAdd);
		string GetErrorMessage();
	}

	public interface IChainRule
	{
		DateTimeRuleParameter Parameter { get; set; }

		

	}
}
