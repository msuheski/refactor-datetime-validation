using System;

namespace app.refactor_this.rules
{
	public class TimeToLessThanFromTimeRule : IDateTimeRule
	{
		public DateTimeRuleParameter Parameters { get; set; }

		public bool Validate(DateTimeRuleParameter dateTimeRuleParameter, GetLocalizedValueByKey_Behaviour localizationManager, AddApplicationMessage messageAdd)
		{
			var delimiters = new[] { ':' };
			var fromTime = dateTimeRuleParameter.TimeFrom.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
			var fromTimeHours = Convert.ToInt32(fromTime[0]);
			var fromTimeMinutes = Convert.ToInt32(fromTime[1]);

			var toTime = dateTimeRuleParameter.TimeTo.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
			var toTimeHours = Convert.ToInt32(toTime[0]);
			var toTimeMinutes = Convert.ToInt32(toTime[1]);

			if ((toTimeHours < fromTimeHours) || ((toTimeHours == fromTimeHours) && (toTimeMinutes < fromTimeMinutes)))
			{
				messageAdd(localizationManager(GetErrorMessage()).ToString(), MessageType.popup, MessageType.error);
				dateTimeRuleParameter.StopValidating = true;
				return false;
			}
			dateTimeRuleParameter.StopValidating = false;
			return true;
		}

		public string GetErrorMessage()
		{
			return LocalizationKeys.bookperson_totimeislesser;
		}
	}
}