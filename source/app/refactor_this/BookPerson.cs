namespace app.refactor_this
{
	public class BookPerson
	{
		static Requisition active_requisition;

		public static Requisition requisition
		{
			get { return active_requisition ?? (active_requisition = RequisitionDAL.get_currently_active_requisition()); }
		}
	}
}