using System;

namespace app.refactor_this
{
	public class Today
	{
		public static int Year
		{
			get { return DateTime.Now.Year; }
		}
	}
}