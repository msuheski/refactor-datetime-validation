using System;
using System.Data;
using System.Data.SqlClient;

namespace app.refactor_this
{
	public class RequisitionDAL
	{
		public static Requisition get_currently_active_requisition()
		{
			var connection = new SqlConnection("data source=(local);Integrated Security=SSPI;Initial Catalog=errors");
			connection.Open();
			var command = connection.CreateCommand();
			command.CommandText = "SELECT * FROM Requisitions";
			command.CommandType = CommandType.Text;

			try
			{
				var reader = command.ExecuteReader();
				var table = new DataTable();
				table.Load(reader);

				var requisition = new Requisition();
				var row = table.Rows[0];

				if (row["DateFrom"] != null) requisition.DateFrom = Convert.ToDateTime(row["DateFrom"]);
				if (row["DateTo"] != null) requisition.DateFrom = Convert.ToDateTime(row["DateTo"]);

				return requisition;
			}
			catch (Exception ex)
			{
				LogManager.log_error("Requisition Fetching", "Failed to get currently active requisition");
				return null;
			}
			finally
			{
				connection.Close();
			}
		}
	}
}