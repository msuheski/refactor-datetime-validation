﻿using System.Web;

namespace app.web.core.stubs
{
  public class StubRequestFactory : ICreateRequests
  {
    public IContainRequestInformation create_request_from(HttpContext context)
    {
      return new StubRequest();
    }

    class StubRequest : IContainRequestInformation
    {
      public string request_type { get; set; }
    }
  }
}