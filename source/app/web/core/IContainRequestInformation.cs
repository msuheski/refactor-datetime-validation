﻿namespace app.web.core
{
  public interface IContainRequestInformation
  {
	  string request_type { get; set; }
  }
}